#include "node.h"

Node::Node() :
point_(0, 0), parent_(nullptr),
f_(0), g_(0), h_(0), 
blocked_(false)
{
}

Node::~Node()
{
	parent_ = nullptr;
}

// Initialise the node
void Node::Init(int x, int y)
{
	InitPoint(x, y);
	parent_ = nullptr;
	neighbours_.clear();
	blocked_ = false;
	f_ = 0;
	g_ = 0;
	h_ = 0;
}

// Initialise the node's point
void Node::InitPoint(int x, int y)
{
	point_.x = x;
	point_.y = y;
}

// Add to node's neighbours
void Node::AddNeighbour(Node* neighbour)
{
	neighbours_.push_back(neighbour);
}

// Display all node data
void Node::DisplayNode()
{
	cout << "Displaying Node at Point: " << point_.x << ", " << point_.y << endl;
	cout << "Address is " << this << endl;
	cout << "Node scores are: f = " << f_ << ", g = " << g_ << ", h = " << h_ <<endl;
	cout << "Blocked? : " << blocked_ << endl;
	if (parent_ != nullptr)
		cout << "Node's Parent is at: " << parent_->point_.x << ", " << parent_->point_.y << endl;
	else cout << "Node has no parent" << endl;
	for (auto it = neighbours_.begin(); it != neighbours_.end(); it++)
	{
		cout << "Neighbour is at: " << (*it)->point_.x << ", " << (*it)->point_.y << endl;
	}
	cout << "\n" << endl;
}

