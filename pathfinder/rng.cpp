#include "rng.h"

RNG::RNG()
{
	srand(static_cast<unsigned int>(time(0)));		// seed RNG
}

RNG::~RNG()
{
}

// return a random number
int RNG::Random()
{
	int r = rand();
	return r;
}

// return a random number within provided range (inclusive)
int RNG::RandomRange(int min, int max)
{
	int range = max - min;
	int r = (Random() % range) + min;

	return r;
}