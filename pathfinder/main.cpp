// Pathfinder
// Created 09/12/2015 by Ross McDonald
// An application which demonstrates pathfinding techniques using the Lee and A* algorithms

#include <iostream>
#include "rng.h"
#include "map.h"
#include "pathfinder_astar.h"
#include "pathfinder_lee.h"

using std::cout;
using std::cin;
using std::endl;

void DisplayGreeting();
void DisplayStartAndEnd(Node& start, Node& end);

char ChoosePathfinder();
bool Restart();

int main()
{
	// Declarations
	RNG rng;
	Map map(rng, 10);
	PathfinderAStar pathfinder_astar(map);
	PathfinderLee pathfinder_lee(map);
	bool quit = false;

	// Welcome
	DisplayGreeting();

	// Main loop
	while (!quit)
	{
		// Initialise the map
		map.Init();
		// Show the map
		map.DisplayMap(Map::BLOCKED);

		// Pick a start node
		Node& startNode = map.GetRandomNode();
		// Pick an end node
		Node& endNode = map.GetRandomNode();
		// Show start and end node
		DisplayStartAndEnd(startNode, endNode);

		// Declare a stack to hold the path
		stack<Node*> thePath;

		// Run pathfinding based on user's choice.
		switch (ChoosePathfinder())
		{
			case 'l':
			{
				thePath = pathfinder_lee.GetPath(startNode, endNode);				
				break;
			}
			case 'a':
			{
				// Start pathfinding
				thePath = pathfinder_astar.GetPath(startNode, endNode);
				break;
			}
			
			default:
				cout << "Something went wrong choosing a pathfinder..." << endl;
		}

		// Display the map after pathfinding
		system("cls");
		map.DisplayPathOnMap(thePath, startNode, endNode);
		
		// check if user wants to do more pathfinding
		if (Restart())
		{
			system("cls");
			cout << "Ok, preparing a new map...\n" << endl;
			continue;
		}
		else
		{
			cout << "Ok, bye." << endl;
			quit = true;
		}
	}
	return 0;
}

// Display a greeting on starting the application
void DisplayGreeting()
{
	cout << "Welcome to Pathfinder." << endl;
	cout << "This application produces a map with some randomly blocked points." << endl;
	cout << "A start point and end point are chosen randomly." << endl;
	cout << "A path is then found between the two points, using either \n";
	cout << "the Lee or A* algorithm.\n" << endl;
}

// Display the start and the end node
void DisplayStartAndEnd(Node& start, Node& end)
{
	cout << "We need a path between (" << start.point().x << ", " << start.point().y <<
		") and (" << end.point().x << ", " << end.point().y << ").\n" << endl;
}

// Get the user's choice of pathfinder
char ChoosePathfinder()
{
	char response;
	do 
	{
		cout << "What kind of pathfinding do you want to use?" << endl;
		cout << "Enter 'l' to use Lee Algorithm Pathfinding." << endl;
		cout << "Enter 'a' to use A* Algorithm Pathfinding." << endl;
		cin >> response;													// enter response
		if ((response != 'l') && (response != 'a'))							// validate response
		{
			cout << "Invalid entry..." << endl;
		}
	} while ((response != 'l') && (response != 'a'));

	return response;														// return valid response
}

// Give the user the option to restart or quit
bool Restart()
{
	char response;
	do 
	{
		cout << "Do you want to do more pathfinding?" << endl;
		cout << "Enter 'y' for yes, 'n' for no." << endl;
		cin >> response;													// enter response
		if ((response != 'y') && (response != 'n'))							// validate response
		{
			cout << "Invalid entry..." << endl;
		}
	} while ((response != 'y') && (response != 'n'));

	if (response == 'y')													// return true for y, false for n
		return true;
	else
		return false;
}
