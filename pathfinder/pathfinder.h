#ifndef _PATHFINDER_H
#define _PATHFINDER_H

#include <chrono>
#include <thread>
#include <stack>
#include "map.h"
#include "node.h"

using std::chrono::duration_cast;
using std::chrono::milliseconds;
using std::this_thread::sleep_for;
using std::stack;

// Pathfinder is the absract base class for pathfinders.
class Pathfinder
{
public:
	Pathfinder(Map& map);
	virtual ~Pathfinder();

	// Find and return a path between start and end
	virtual stack<Node*> GetPath(Node& start, Node& end) = 0;

protected:
	Map& mapRef_;		// reference to the map
	
};


#endif // !_PATHFINDER_H
