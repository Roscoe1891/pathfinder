#ifndef _PATHFINDER_LEE_H
#define _PATHFINDER_LEE__H

#include "pathfinder.h"

class PathfinderLee : public Pathfinder
{
public:
	PathfinderLee(Map& mapRef);
	~PathfinderLee();

	// Find and return path between start and end
	virtual stack<Node*> GetPath(Node& start, Node& end);

private:

};


#endif // !_PATHFINDER_LEE
