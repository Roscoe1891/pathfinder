#include "pathfinder_astar.h"

PathfinderAStar::PathfinderAStar(Map& map) :
	Pathfinder(map)
{
}

PathfinderAStar::~PathfinderAStar()
{
}

// Find and return path between start and end
stack<Node*> PathfinderAStar::GetPath(Node& startNode, Node& endNode)
{
	cout << "Starting pathfinding using A* Algorithm..." << endl;
	
	stack<Node*> path;					// final path
	list<Node*> open;					// open set
	unordered_set<Node*> closed;		// closed set

	// set values for start node
	startNode.set_g(0);
	startNode.set_h(GetHeuristic(startNode, endNode));
	startNode.set_f(startNode.g() + startNode.h());

	// add start node  to open set
	InsertOrdered(&open, &startNode);

	// while open set is not empty
	while (!open.empty())
	{
		Node* current = *open.begin();				// current is the node at the front of the open set

		if (current == &endNode)					// if current isn't the end node
		{
			std::cout << "found the end!" << endl;
			break;
		}

		open.pop_front();							// remove the element from the front of the open set
		closed.insert(current);						// add current to closed set

		// check neighbours of current
		for (auto it_n = current->neighbours()->begin(); it_n != current->neighbours()->end(); it_n++)
		{
			// if neighbour is in the closed set
			if (closed.find(*it_n) != closed.end())
			{
				// node has already been checked, move on
				continue;
			}
			// if neighbour is blocked
			if ((*it_n)->blocked())
			{
				continue;
			}

			// declare g score for neighbour
			int new_g = current->g() + GetDistanceBetween(*current, **it_n);

			// if neighbour is already in the open list
			list<Node*>::iterator it_open = LinearSearch(&open, (*it_n));
			if (it_open != open.end())
			{
				// ignore if we have already found a shorter path
				if (new_g > (*it_n)->g())
				{
					continue;
				}
				// otherwise remove neighbour from the open list
				else
				{
					open.erase(it_open);
				}
			}

			// set neighbour values
			(*it_n)->set_g(new_g);
			(*it_n)->set_h(GetHeuristic(**it_n, endNode));
			(*it_n)->set_f((*it_n)->g() + (*it_n)->h());
			(*it_n)->set_parent(current);

			// add to open list
			InsertOrdered(&open, (*it_n));

			// show progress
			system("cls");
			cout << "Finding a path between (" << startNode.point().x << ", " << startNode.point().y << ") and (";
			cout << endNode.point().x << ", " << endNode.point().y << ") using A* algorithm..." << endl;
			mapRef_.DisplayMap(Map::F_SCORE);
			sleep_for(milliseconds(2000));

		}
	}

	// prepare final path:
	// start at end node
	Node* node = &endNode;
	while (node != &startNode)
	{
		// add node to list
		path.push(node);
		if (node->parent() != nullptr)
		{
			// look at node's parent next
			node = node->parent();
		}
		else
			cout << "Added a node with no parent to the list." << endl;
	}

	// add the start node
	path.push(&startNode);
	
	return path;
}

// Get the heuristic value for supplied node in relation to supplied end node
int PathfinderAStar::GetHeuristic(Node& node, Node& endNode)
{
	int h = 0;

	// heuristic using Euclidean Distance
	int diff_x = node.point().x - endNode.point().x;
	int diff_y = node.point().y - endNode.point().y;
	double diff_x_sq = pow(diff_x, 2);
	double diff_y_sq = pow(diff_y, 2);
	int unit_dist = (int)sqrt(diff_x_sq + diff_y_sq);

	h = DISTANCE * unit_dist;

	return h;
}

// Get the distance between a node and its neighbour
int PathfinderAStar::GetDistanceBetween(Node& node, Node& neighbour)
{
	int dist = 0;
	if ((node.point().x == neighbour.point().x) || (node.point().y == neighbour.point().y))
		dist = DISTANCE;
	else
		dist = DIAGONAL_DISTANCE;
	return dist;
}

// add provided node to provided list, in order of f score
void PathfinderAStar::InsertOrdered(list<Node*>* nodeList, Node* item)
{
	list<Node*>::const_iterator it = nodeList->begin();
	while (it != nodeList->end())
	{
		if ((*it)->f() > item->f())
		{
			nodeList->insert(it, item);
			return;
		}
		it++;
	}
	nodeList->push_back(item);
}

// search for provided node in provided list
// returns iterator to the node if found
// returns list.end() if not found
list<Node*>::iterator PathfinderAStar::LinearSearch(list<Node*>* nodeList, Node* item)
{
	for (list<Node*>::iterator it = nodeList->begin(); it != nodeList->end(); it++)
	{
		if ((*it) == item)
		{
			return it;
		}
	}
	return nodeList->end();
}

// Print the provided list of nodes (for debugging)
void PathfinderAStar::PrintNodeList(list<Node*>* nodeList)
{
	cout << "open list contains:" << endl;
	for (list<Node*>::const_iterator it = nodeList->begin(); it != nodeList->end(); it++)
	{
		(*it)->DisplayNode();
	}
}

