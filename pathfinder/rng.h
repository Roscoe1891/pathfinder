#ifndef _RNG_H
#define _RNG_H

#include <iostream>
#include <ctime>

// The RNG class provides a random number generator

class RNG
{
public:
	RNG();
	~RNG();

	int Random();							// return a random number
	int RandomRange(int min, int max);		// return a random number within provided range (inclusive)

};

#endif