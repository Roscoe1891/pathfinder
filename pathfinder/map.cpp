#include "map.h"

Map::Map(RNG& rng, int blockedNodes) :
rng_ref_(rng), kBlocked_nodes_(blockedNodes)
{
}

Map::~Map()
{
}

// Initialise the map
void Map::Init()
{
	//cout << "Initialising Map..." << endl;
	InitNodes();
	InitNodeNeighbours();
	InitBlockedNodes();
	TidyBlockedNodes();
	//cout << "Map is ready.\n" << endl;
}

// Initialise the nodes 
void Map::InitNodes()
{
	//cout << "Initialising Nodes..." << endl;
	for (int y = 0; y < HEIGHT; ++y)
	{
		for (int x = 0; x < WIDTH; ++x)
		{
			nodeMap_[y][x].Init(x, y);
		}
	}
}

// Set the neighbours for each node in the node array
void Map::InitNodeNeighbours()
{
	//cout << "Initialising Node Neighbours..." << endl;
	for (int y = 0; y < HEIGHT; ++y)
	{
		for (int x = 0; x < WIDTH; ++x)
		{
			if ((y - 1) >= 0)
				nodeMap_[y][x].AddNeighbour(&nodeMap_[y - 1][x]);
			if ((y + 1) < HEIGHT)
				nodeMap_[y][x].AddNeighbour(&nodeMap_[y + 1][x]);
			if ((x - 1) >= 0)
				nodeMap_[y][x].AddNeighbour(&nodeMap_[y][x - 1]);
			if ((x + 1) < WIDTH)
				nodeMap_[y][x].AddNeighbour(&nodeMap_[y][x + 1]);
			if (((x + 1) < WIDTH) && ((y + 1) < HEIGHT))
				nodeMap_[y][x].AddNeighbour(&nodeMap_[y + 1][x + 1]);
			if (((x + 1) < WIDTH) && ((y - 1) >= 0))
				nodeMap_[y][x].AddNeighbour(&nodeMap_[y - 1][x + 1]);
			if (((x - 1) >= 0) && ((y + 1) < HEIGHT))
				nodeMap_[y][x].AddNeighbour(&nodeMap_[y + 1][x - 1]);
			if (((x - 1) >= 0) && ((y - 1) >= 0))
				nodeMap_[y][x].AddNeighbour(&nodeMap_[y - 1][x - 1]);
			//nodeMap_[y][x].DisplayNode();
		}
	}
}

// Set the blocked nodes for the map
void Map::InitBlockedNodes()
{
	//cout << "Initialising Blocked Nodes..." << endl;
	for (int i = 0; i < kBlocked_nodes_; ++i)
	{
		Node& blocked = GetRandomNode();		// get a random node
		blocked.set_blocked(true);				// node is blocked
	}
}

// 'Tidies' the map so that blocked nodes form a solid shape
void Map::TidyBlockedNodes()
{
	//cout << "Tidying Blocked Nodes..." << endl;
	// check all nodes
	for (int y = 0; y < HEIGHT; y++)
	{
		for (int x = 0; x < WIDTH; x++)
		{
			// if node is blocked
			if (nodeMap_[y][x].blocked())
			{
				// if Up Left diagonal is blocked
				if (nodeMap_[y - 1][x - 1].blocked())
				{
					// if Up and Left are not blocked, block Up
					if ((!nodeMap_[y - 1][x].blocked()) && (!nodeMap_[y][x - 1].blocked()))
					{
						nodeMap_[y - 1][x].set_blocked(true);
					}
				}
				// check Up Right diagonal
				if (nodeMap_[y - 1][x + 1].blocked())
				{
					// if Up and Right are not blocked, block Right
					if ((!nodeMap_[y - 1][x].blocked()) && (!nodeMap_[y][x + 1].blocked()))
					{
						nodeMap_[y][x + 1].set_blocked(true);
					}
				}
				// check Bottom Left diagonal
				if (nodeMap_[y + 1][x - 1].blocked())
				{
					// if Bottom and Left are not blocked, block Left
					if ((!nodeMap_[y][x - 1].blocked()) && (!nodeMap_[y + 1][x].blocked()))
					{
						nodeMap_[y][x - 1].set_blocked(true);
					}
				}
				// check Bottom Right diagonal
				if (nodeMap_[y + 1][x + 1].blocked())
				{
					// if Bottom and Right are not blocked, block Bottom
					if ((!nodeMap_[y][x + 1].blocked()) && (!nodeMap_[y + 1][x].blocked()))
					{
						nodeMap_[y + 1][x].set_blocked(true);
					}
				}
			}
		}
	}
}

// Display the map using the style provided
void Map::DisplayMap(DisplayType displayType)
{
	cout << "Here is the Map, ";
	switch (displayType)
	{
		// explain what is being displayed
		case CO_ORD:
			cout << "showing the co-ordinates of each node." << endl;
			break;
		case F_SCORE:
			cout << "showing the scores for each node." << endl;
			break;
		case BLOCKED:
			cout << "showing blocked nodes." << endl;
			break;
	}

	for (int y = 0; y < HEIGHT; ++y) 
	{
		cout << " | ";
		for (int x = 0; x < WIDTH; ++x) 
		{
			switch (displayType)
			{
			// Display co-ordinates of node
			case CO_ORD:
				cout << nodeMap_[y][x].point().x << ", " << nodeMap_[y][x].point().y << " | ";
				break;
			// Display f score of node
			case F_SCORE:
				if (nodeMap_[y][x].blocked())
					cout << "  X | ";
				else if (nodeMap_[y][x].f() < 10)
					cout << "  " << nodeMap_[y][x].f() << " | ";
				else if (nodeMap_[y][x].f() < 100)
					cout << " " << nodeMap_[y][x].f() << " | ";
				else 
					cout << nodeMap_[y][x].f() << " | ";
				break;
			// display 0 for clear nodes, X for blocked nodes
			case BLOCKED:
				if (!nodeMap_[y][x].blocked())
					cout << "  0 | ";
				else 
					cout << "  X | ";
				break;
			}
		}
		cout << "\n" << endl;
	}
	cout << "\n" << endl;
}

// Dipslay path between start and end on the map
void Map::DisplayPathOnMap(stack<Node*> path, Node& start, Node& end)
{
	// declare a temporary array, all values set to 0
	int map[HEIGHT][WIDTH];

	for (int y = 0; y < HEIGHT; ++y)
	{
		for (int x = 0; x < WIDTH; ++x)
		{
			map[y][x] = 0;
		}
	}

	// step through the path, setting the corresponding point to the number of the current step in the path
	// starts at 1 (as 0 is indistinguishable from non-blocked nodes)
	int i = 1;
	while (!path.empty())
	{
		map[path.top()->point().y][path.top()->point().x] = i;
		path.pop();
		++i;
	}
	
	// Exaplin what is being shown
	cout << "\nHere is the Map," << endl;
	cout << "showing the path from: (";
	cout << start.point().x << ", "<< start.point().y << "), to ("<< end.point().x << ", " << end.point().y << ")" << endl;

	// Display the temporary array showing the path on the map 
	// against blocked and  non-blocked nodes
	for (int y = 0; y < HEIGHT; ++y)
	{
		cout << " | ";
		for (int x = 0; x < WIDTH; ++x)
		{
			if (nodeMap_[y][x].blocked())
				cout << "  " << "X" << " | ";
			else if (map[y][x] < 10)
				cout << "  " << map[y][x] << " | ";
			else
				cout << " " << map[y][x] << " | ";
		}
		cout << "\n" << endl;
	}
	cout << "\n" << endl;
}

// Return the node at the provided point
Node& Map::GetNodeAtPoint(const Point point)
{
	Node& node = nodeMap_[point.y][point.x];
	return node;
}

// Return a random non-blocked node
Node& Map::GetRandomNode()
{
	//cout << "Getting a random node" << endl;
	bool foundNode = false;
	Node* n = &nodeMap_[rng_ref_.RandomRange(0, HEIGHT - 1)][rng_ref_.RandomRange(0, WIDTH - 1)];
	
	while (!foundNode)
	{
		if (!n->blocked())
		{
			foundNode = true;
			break;
		}
		else
		{
			n = &nodeMap_[rng_ref_.RandomRange(0, HEIGHT - 1)][rng_ref_.RandomRange(0, WIDTH - 1)];
		}
	}

	Node& node = *n;
	//node.DisplayNode();
	return node;
}

