#include "pathfinder_lee.h"

PathfinderLee::PathfinderLee(Map& mapRef) :
	Pathfinder(mapRef)
{
}

PathfinderLee::~PathfinderLee()
{
}

// Find and return path between start and end
stack<Node*> PathfinderLee::GetPath(Node& startNode, Node& endNode)
{
	cout << "Starting pathfinding using Lee Algorithm..." << endl;

	// start the check at 1, at the start node
	int check = 1;				
	startNode.set_f(check);

	// score cells
	bool foundEnd = false;
	while (!foundEnd)
	{

		// loop through all cells
		for (int y = 0; y < HEIGHT; ++y)
		{
			for (int x = 0; x < WIDTH; ++x)
			{
				// check the node at this point
				Node& current = mapRef_.GetNodeAtPoint(Point(x, y));
				
				if ((current.f() == check))		// if current node f score is same value as check
				{
						// check current's neighbours
						for (auto it = current.neighbours()->begin(); it != current.neighbours()->end(); it++)
						{
							// if neighbour hasn't been scored and isn't blocked
							if (((*it)->f() == 0) && (!(*it)->blocked()))
							{
								(*it)->set_f(check + 1);	// score neighbour
								// Print progress
								system("cls");
								cout << "Finding a path between (" << startNode.point().x << ", " << startNode.point().y << ") and (";
								cout << endNode.point().x << ", " << endNode.point().y << ") using Lee algorithm..." << endl;
								mapRef_.DisplayMap(Map::F_SCORE);
								sleep_for(milliseconds(2000));
							}
							// if this neighbour is the end point
							if ((*it) == &endNode)
							{
								foundEnd = true;		// found the end
								break;
							}
						}
				}
			}
		}
		check++;
	}

	stack<Node*> path;					// make path
	path.push(&endNode);				// push end node onto to the path
	Node* current = path.top();			// this is the current node

	bool foundStart = false;
	while (!foundStart)
	{
		// if start is found
		if (current == &startNode)
		{
			foundStart = true;		// path is complete
			continue;
		}

		// check current's neighbours
		for (auto it = current->neighbours()->begin(); it != current->neighbours()->end(); it++)
		{
			// if neighbour's score is 1 less than current's, add to path
			if ((*it)->f() == (current->f() - 1))
			{
				path.push((*it));
				break;
			}
		}
		current = path.top();
	}
	return path;

}
