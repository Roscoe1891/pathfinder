#ifndef _NODE_H
#define _NODE_H

#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

// Point Struct
struct Point
{
	int x, y;
	Point(int x = 0, int y = 0) : x(x), y(y)
	{}
};

// Nodes are the main navigational elements used for pathfinding
// The map is comprised of nodes

class Node
{
public:
	Node();
	~Node();
	// Intialise Node 
	void Init(int x, int y);
	void InitPoint(int x, int y);
	void AddNeighbour(Node* neighbour);

	// Display node
	void DisplayNode();

	// @ return point
	inline const Point& point() { return point_; }
	// @ param[in] new f_
	inline void set_f(int f) { f_ = f; }
	// @ return f_
	inline const int& f() { return f_; }
	// @ param[in] new g_
	inline void set_g(int g) { g_ = g; }
	// @ return g_
	inline const int& g() { return g_; }
	// @ param[in] new h_
	inline void set_h(int h) { h_ = h; }
	// @ return h_
	inline const int& h() { return h_; }
	// @ return neighbours
	inline vector<Node*>* neighbours() { return &neighbours_; }
	// @ return parent
	inline Node* parent() { return parent_; }
	// @ param[in] parent
	inline void set_parent(Node* parent) { parent_ = parent; }
	// @ return blocked
	inline const bool& blocked() { return blocked_; }
	// @param[in] blocked
	inline void set_blocked(bool blocked) { blocked_ = blocked; }

private:
	Point point_;
	Node* parent_;
	vector<Node*> neighbours_;
	int f_, g_, h_;
	bool blocked_;

};



#endif