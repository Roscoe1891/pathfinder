#ifndef _MAP_H
#define _MAP_H

#include "node.h"
#include "rng.h"
#include <stack>
#include <iostream>

// height and width of the map
#define HEIGHT 10
#define WIDTH 10
// distance between nodes on the map
#define DISTANCE 10
#define DIAGONAL_DISTANCE 14

using std::cout;
using std::endl;
using std::stack;

// The map class contains the array nodes which represents the world
// It is responsible for intialising this array

class Map
{
public:
	enum DisplayType { CO_ORD = 0, F_SCORE, BLOCKED };

	Map(RNG& rng, int blockedNodes);
	~Map();

	void Init();
	
	// Display the map (style of display dependent on supplied type)
	void DisplayMap(DisplayType displayType);
	// Display the provided path between start and end on the map
	void DisplayPathOnMap(stack<Node*> path, Node& start, Node& end);
	
	// Return node @ provided point
	Node& GetNodeAtPoint(const Point point);
	// Return a random node
	Node& GetRandomNode();

private:
	// internal initialisation functions
	void InitNodes();
	void InitNodeNeighbours();
	void InitBlockedNodes();
	void TidyBlockedNodes();

	Node nodeMap_[HEIGHT][WIDTH];		// array of nodes representing the world
	RNG& rng_ref_;						// random number generator reference
	const int kBlocked_nodes_;			// the number of blocked nodes on the map

};

#endif